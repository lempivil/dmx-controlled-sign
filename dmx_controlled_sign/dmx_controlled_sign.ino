#include <Conceptinetics.h>    // https://sourceforge.net/projects/dmxlibraryforar/
#include <FastLED.h>    // https://github.com/FastLED/FastLED
#include <EEPROM.h>   // https://www.arduino.cc/en/Reference/EEPROM

#define DATA_OUT_PIN 5    // output pin for led signaling
#define NUM_LEDS 150    // 30 leds/m * 5m
#define DMX_SLAVE_CHANNELS 6    // Dimmer, Effect, Speed, R, G, B
#define PSU_MAX_POWER 8000    // mA
#define STRIP_VOLTAGE 5
#define EFFECT1_LOWER_LIMIT 0
#define EFFECT1_HIGHER_LIMIT 0
#define EFFECT2_LOWER_LIMIT (EFFECT1_HIGHER_LIMIT + 1)
#define EFFECT2_HIGHER_LIMIT 50
#define EFFECT3_LOWER_LIMIT (EFFECT2_HIGHER_LIMIT + 1)
#define EFFECT3_HIGHER_LIMIT 100
#define EFFECT4_LOWER_LIMIT (EFFECT3_HIGHER_LIMIT + 1)
#define EFFECT4_HIGHER_LIMIT 150
#define EFFECT5_LOWER_LIMIT (EFFECT4_HIGHER_LIMIT + 1)
#define EFFECT5_HIGHER_LIMIT 200
#define EFFECT6_LOWER_LIMIT (EFFECT5_HIGHER_LIMIT + 1)
#define EFFECT6_HIGHER_LIMIT 250
#define EFFECT7_LOWER_LIMIT (EFFECT6_HIGHER_LIMIT + 1)
#define EFFECT7_HIGHER_LIMIT 256
#define MAX_FADE 80
#define MIN_FADE 0

static const uint8_t DMX_CHANNEL_SELECTOR_PINS[9] = {6, 7, 8, 9, 10, 11, 12, 13, 4};
static unsigned short start_channel = 0;    // DMX channel (first DMX address light listens to. Will likely be overridden in void setup())
unsigned short i = 0;    // Global index for movement effects

CRGB leds[NUM_LEDS];
DMX_Slave dmx_slave(DMX_SLAVE_CHANNELS );
RDM_Responder rdm_responder(0x0707, 0x1, 0x2, 0x3, 0x4, dmx_slave );    // ManufacturerID 0707h "Conceptinetics Technologies and Consultancy Ltd.", serial no. 1234

void setup()  {  
  FastLED.addLeds<NEOPIXEL, DATA_OUT_PIN>(leds, NUM_LEDS);
  FastLED.setMaxPowerInVoltsAndMilliamps(STRIP_VOLTAGE, PSU_MAX_POWER); 

  // Read dip switch to determine channel
  // NOTE: Currently start_channel is read only upon startup/reset. This could be modified to change channels also mid-operation by moving IF-loop in FOR-loop below into void loop() part of the code
  for(uint8_t i = 0; i < sizeof(DMX_CHANNEL_SELECTOR_PINS)/sizeof(DMX_CHANNEL_SELECTOR_PINS[1]);++i){
    pinMode(DMX_CHANNEL_SELECTOR_PINS[i], INPUT_PULLUP);
    if(digitalRead(DMX_CHANNEL_SELECTOR_PINS[i]) == LOW){
      start_channel |= 1<<i;
    }
  }

  dmx_slave.onReceiveComplete(OnFrameReceiveComplete);
  dmx_slave.enable();

  if(start_channel == 0){
    rdm_responder.setDeviceInfo(0x1, rdm::CategoryFixtureOther, 1, 1);    // RDM device info (Device model ID = 1, Category, no. personalities, current personality)
    rdm_responder.setSoftwareVersionId(0x00, 0x00, 0x00, 0x01);
    rdm_responder.onDMXStartAddressChanged(DMXAddressChangedThroughRDM);
    rdm_responder.enable(); 
    EEPROM.get(0, start_channel);
    if(start_channel == 0){       // Fallback in case of channel has not been ever written to eeprom. This should happen only after flashing new software into a device
      start_channel = 1; 
      EEPROM.put(0, start_channel);
    }
  }

  dmx_slave.setStartAddress(start_channel);
}


void loop() {
  
  //
  // Effect 1 (DMX effect channel value 0), static color
  //
  if ( dmx_slave.getChannelValue(2) == EFFECT1_HIGHER_LIMIT){

    // Iterate through all leds and set colors as defined in channels 4, 5 and 6. Nothing special here
    for (unsigned short i = 0; i < NUM_LEDS; i++){
      leds[i].setRGB( dmx_slave.getChannelValue(4), dmx_slave.getChannelValue(5), dmx_slave.getChannelValue(6));
    }    
  }

  //
  // Effect 2 (DMX  effect channel value 1 - 50), flow
  //
  else if ( dmx_slave.getChannelValue(2) >= EFFECT2_LOWER_LIMIT and dmx_slave.getChannelValue(2) <= EFFECT2_HIGHER_LIMIT){

    // If index is in allowable values
    if(i < NUM_LEDS){
      Advance(i, EFFECT2_LOWER_LIMIT, EFFECT2_HIGHER_LIMIT);
      if(i < NUM_LEDS - 1){
        leds[i + 1].setRGB( dmx_slave.getChannelValue(4)/2, dmx_slave.getChannelValue(5)/2, dmx_slave.getChannelValue(6)/2);
      }
      i++;
    } else {
      // If index in in the end, reset index
      i = 0;
    }
  }

  //
  // Effect 3 (DMX  effect channel value 51 - 100), reverse flow
  //
  else if ( dmx_slave.getChannelValue(2) >= EFFECT3_LOWER_LIMIT and dmx_slave.getChannelValue(2) <= EFFECT3_HIGHER_LIMIT){
    
    if(i > 0){
      Advance(i, EFFECT3_LOWER_LIMIT, EFFECT3_HIGHER_LIMIT);
      if(i > 1){
        leds[i - 1].setRGB( dmx_slave.getChannelValue(4)/2, dmx_slave.getChannelValue(5)/2, dmx_slave.getChannelValue(6)/2);
      }
      i--;
    } else {
      i = NUM_LEDS - 1;
    }
  }

  //
  // Effect 4 (DMX  effect channel value 101 - 150)
  //
  else if ( dmx_slave.getChannelValue(2) >= EFFECT4_LOWER_LIMIT and dmx_slave.getChannelValue(2) <= EFFECT4_HIGHER_LIMIT){
    if(i > 0){
      Advance(random8(NUM_LEDS), EFFECT4_LOWER_LIMIT, EFFECT4_HIGHER_LIMIT);
    }
  }

  //
  // Effect 5 (DMX  effect channel value 151 - 200)
  //
  else if ( dmx_slave.getChannelValue(2) >= EFFECT5_LOWER_LIMIT and dmx_slave.getChannelValue(2) <= EFFECT5_HIGHER_LIMIT){
    // Tähän vertical flow?
  }

  //
  // Effect 6 (DMX  effect channel value 201 - 250)
  //
  else if ( dmx_slave.getChannelValue(2) >= EFFECT6_LOWER_LIMIT and dmx_slave.getChannelValue(2) <= EFFECT6_HIGHER_LIMIT){
    // Tähän horizontal flow?
  }

  //
  // Effect JOKERI (DMX  effect channel value 251 - 255)
  //
  else if ( dmx_slave.getChannelValue(2) > EFFECT7_LOWER_LIMIT){
    // To be defined?
  }
} 

// Interrupt handler to get master-brightness and update it to led
void OnFrameReceiveComplete(void){
  FastLED.setBrightness(dmx_slave.getChannelValue(1));
  FastLED.show();
}

// Handler to save DMX address set through RDM to persistent memory 
void DMXAddressChangedThroughRDM(void){
  EEPROM.put(0, dmx_slave.getStartAddress());
}

//  Make a step in movement effects
void Advance( int i, uint8_t scaling_low_limit, uint8_t scaling_high_limit){
    fadeToBlackBy(leds, NUM_LEDS, map( dmx_slave.getChannelValue(2), scaling_low_limit, scaling_high_limit, MAX_FADE, MIN_FADE));         // Fade all leds a bit (to achieve "movement" in such effects)
    leds[i].setRGB( dmx_slave.getChannelValue(4), dmx_slave.getChannelValue(5), dmx_slave.getChannelValue(6));              // Write led value to active led in index i
    delay(255 - dmx_slave.getChannelValue(3));                                              // Wait some time to achieve speed control (TODO: Not the best way to do this but maybe will be fixed later)
}
