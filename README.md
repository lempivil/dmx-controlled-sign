# DMX controlled Neopixel project

-------------

Created by Ville Lempiäinen (ville.lempiainen@hotmail.com)

   Created 26.9.2018  
   Update 26.10.2019  
   Update 20.02.2021  

---------------------

Control individually addressable leds using DMX. 

Required hardware:  
    1. Arduin uno  
    2. CTC-DRA-13-R2 DMX shield  
    3. Neopixel led strip \*  
    4. Adequate power supply \*\*  
    5. 1000 μF capacitor + 300-500 Ohm resistor as suggested in https://learn.adafruit.com/adafruit-neopixel-uberguide?view=all  

\* WS2812B or similar with 5V data input. Usage of other signal voltages may be possible using logic level shifter or other hardware  
\*\* voltage and amperage defined by used hardware. At least [Arduino documentation] and strip voltage must be taken into account. If voltage is higher than 5V, step-down converter is needed for Arduino

   NOTE: Software may or may not support other confiqurations. Above mentioned confiquration is currently the only one that has been tested.  
   NOTE2: Nowadays there is individually addressable 12V strips with 5V logic, such as WS2815. They are a wise solution for bigger installations due to power requirements. Arduino can possibly be powered from the 12V source using step-down converter

---------------------

DMX Channel definitions:  
1. Dimmer  
2. Effect
3. Speed (0 is the slowest and 255 fastest)  
4. Red  
5. Green  
6. Blue  

---------------------

![Schematics](/Documentation/Electrical_connections.svg)
![Image1](/Documentation/Photo1.jpg) | ![Image2](/Documentation/Photo2.jpg)

---------------------

If you are willing to review or otherwise improve the code, that would be great! All help is needed.

---------------------

Available for use per MIT License  
  
Copyright 2019 Ville Lempiäinen (ville.lempiainen@hotmail.com)  
  
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  
  
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.  


[Arduino documentation]: https://store.arduino.cc/arduino-uno-rev3